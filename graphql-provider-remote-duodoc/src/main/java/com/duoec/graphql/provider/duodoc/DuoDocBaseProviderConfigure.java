package com.duoec.graphql.provider.duodoc;

import com.duoec.graphql.provider.BaseProviderRegistry;
import com.duoec.graphql.provider.dto.DuoDocGraphqlProviderServiceInfo;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 注册当前服务为Graphql Provider
 *
 * @author xuwenzhen
 * @date 2019/4/16
 */
@Component
public class DuoDocBaseProviderConfigure extends BaseProviderRegistry {
    private static final Logger logger = LoggerFactory.getLogger(DuoDocBaseProviderConfigure.class);

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json;charset=UTF-8";
    private static final String HOST = "host";
    private static final int HTTP_CODE_SUCCESS = 200;

    /**
     * Graphql Duo-Doc注册器地址
     *
     * @demo http://127.0.0.1:17040/api/register/duo-doc
     */
    @Value("${graphql.registry.duo-doc-address}")
    private String duoDocRegistryAddress;

    /**
     * Graphql服务的Mesh网格appId
     */
    @Value("${graphql.registry.host:}")
    private String duoDocRegistryHost;

    @Autowired
    private OkHttpClient okHttpClient;

    @PostConstruct
    public void registryService() {
        validate();

        DuoDocGraphqlProviderServiceInfo provider = getDuoDocGraphqlProviderServiceInfo();
        if (provider == null) {
            return;
        }

        //上报
        registerProvider(provider);
    }

    @Override
    protected void registerProvider(DuoDocGraphqlProviderServiceInfo provider) {
        HttpUrl httpUrl = HttpUrl.parse(duoDocRegistryAddress);
        if (httpUrl == null) {
            DuoDocBaseProviderConfigure.logger.error("调用发生错误，url异常:{}", duoDocRegistryAddress);
            return;
        }

        HttpUrl.Builder urlBuilder = httpUrl.newBuilder();
        Request.Builder requestBuilder = new Request.Builder().url(urlBuilder.build());
        requestBuilder.addHeader(DuoDocBaseProviderConfigure.CONTENT_TYPE, DuoDocBaseProviderConfigure.CONTENT_TYPE_VALUE);
        if (!StringUtils.isEmpty(duoDocRegistryHost)) {
            //设置Mesh网格的请求头
            requestBuilder.addHeader(DuoDocBaseProviderConfigure.HOST, duoDocRegistryHost);
        }
        RequestBody requestBody = null;
        try {
            requestBody = MultipartBody.create(provider.toString().getBytes(CHARSET_NAME));
        } catch (UnsupportedEncodingException e) {
            DuoDocBaseProviderConfigure.logger.error("不支持字符编码：{}", CHARSET_NAME, e);
            return;
        }
        requestBuilder.post(requestBody);
        Request request = requestBuilder.build();
        long t1 = System.currentTimeMillis();
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (response.code() != DuoDocBaseProviderConfigure.HTTP_CODE_SUCCESS) {
                DuoDocBaseProviderConfigure.logger.error("注册服务失败！，status:{}", response.code());
                return;
            }

            try (ResponseBody responseBody = response.body()) {
                if (responseBody == null) {
                    DuoDocBaseProviderConfigure.logger.error("注册服务，返回为空！，status:{}", response.code());
                    return;
                }
                DuoDocBaseProviderConfigure.logger.info("注册Duo Graphql Provider:{}, 结果：{}", provider, responseBody.string());
            }
        } catch (IOException e) {
            DuoDocBaseProviderConfigure.logger.error("调用失败：{},{}", e.getMessage(), request.toString(), e);
        } finally {
            DuoDocBaseProviderConfigure.logger.info("{}, 耗时 {}", urlBuilder, System.currentTimeMillis() - t1);
        }
    }
}
