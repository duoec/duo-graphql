package com.duoec.graphql.pipeline;

import com.duoec.graphql.core.GraphqlConsts;
import com.duoec.graphql.core.exception.GraphqlBuildException;
import com.duoec.graphql.core.util.GraphqlTypeUtils;
import com.duoec.graphql.dto.DuoDocBaseProviderApiDto;
import com.duoec.graphql.dto.DuoDocBaseProviderDto;
import com.duoec.graphql.fetcher.DataFetcherProxy;
import com.duoec.graphql.provider.dto.DuoDocGraphqlProviderServiceInfo;
import com.duoec.graphql.provider.dto.provider.Api;
import com.duoec.graphql.provider.dto.provider.EntityRef;
import com.duoec.graphql.provider.dto.provider.ProviderApiDto;
import com.duoec.graphql.register.JsonService;
import com.duoec.graphql.scalar.JsonScalar;
import com.duoec.graphql.service.DuoDocService;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLInputType;
import graphql.schema.GraphQLNonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 远端RestFul接口注入（需要基于DuoDoc文档）
 * 本方法会去扫描classpaths下的*.
 *
 * @author xuwenzhen
 */
@Service
public class DuoDocBaseRestBuildPipeline implements Pipeline {
    private static final Logger logger = LoggerFactory.getLogger(DuoDocBaseRestBuildPipeline.class);

    @Value("${duo.graphql.duodoc.rest.providers:}")
    private String duoDocBassRestProviders;

    @Autowired
    private DuoDocService duoDocService;

    @Autowired
    private JsonService jsonService;

    /**
     * 处理
     *
     * @param registryState 当前注册信息
     */
    @Override
    public void doPipeline(RegistryState registryState) {
        if (Strings.isNullOrEmpty(duoDocBassRestProviders)) {
            return;
        }
        Splitter
                .on(GraphqlConsts.STR_COMMA)
                .omitEmptyStrings()
                .trimResults()
                .split(duoDocBassRestProviders)
                .forEach(configName -> registryDuoDocBaseApis(registryState, configName));
    }

    /**
     * 执行的顺序
     *
     * @return 数值，越小越前
     */
    @Override
    public int order() {
        return 1000;
    }

    private void registryDuoDocBaseApis(RegistryState registryState, String configName) {
        String apiConfig = getResourceFileAsString(configName);
        if (Strings.isNullOrEmpty(apiConfig)) {
            logger.warn("配置文件：{}内容为空，跳过！", configName);
            return;
        }

        DuoDocBaseProviderDto duoDocBaseProvider = jsonService.toObject(apiConfig, DuoDocBaseProviderDto.class);
        if (duoDocBaseProvider == null || CollectionUtils.isEmpty(duoDocBaseProvider.getModules())) {
            logger.warn("配置文件：{}内容异常，跳过！", configName);
            return;
        }

        StringBuilder apiCodes = new StringBuilder();
        duoDocBaseProvider.getModules().forEach(module -> {
            List<DuoDocBaseProviderApiDto> apis = module.getApis();
            if (CollectionUtils.isEmpty(apis)) {
                return;
            }
            apis.forEach(api -> apiCodes.append(GraphqlConsts.STR_COMMA).append(api.getCode()));
        });
        if (apiCodes.length() == 0) {
            logger.warn("配置文件：{} 未指定api.code，跳过！", configName);
            return;
        }
        apiCodes.deleteCharAt(0);
        //faac39aa3ea96ed18db900a24cb187946e1b63b4
        ProviderApiDto providerApiDto = duoDocService.fetchDocData(duoDocBaseProvider.getAppId(), null, apiCodes.toString());
        if (providerApiDto == null) {
            logger.warn("配置文件：{} 拉取接口文档信息失败，appId={}, apiCodes={}！", configName, duoDocBaseProvider.getAppId(), apiCodes.toString());
            return;
        }

        registryDuoDocBaseApis(registryState, duoDocBaseProvider, providerApiDto);
    }

    private void registryDuoDocBaseApis(
            RegistryState registryState,
            DuoDocBaseProviderDto duoDocBaseProvider,
            ProviderApiDto providerApiDto
    ) {
        Map<String, Api> apiMap = Maps.newHashMap();
        providerApiDto.getApis().forEach(api -> apiMap.put(api.getCode(), api));
        registryState.addEntities(providerApiDto.getEntities());

        duoDocBaseProvider.getModules().forEach(module -> module.getApis().forEach(fieldConf -> registryApi(
                registryState, duoDocBaseProvider, apiMap, module.getName(), fieldConf
        )));
    }

    private void registryApi(
            RegistryState registryState,
            DuoDocBaseProviderDto duoDocBaseProvider,
            Map<String, Api> apiMap,
            String moduleName,
            DuoDocBaseProviderApiDto fieldConf
    ) {
        Api api = apiMap.get(fieldConf.getCode());
        if (api == null) {
            logger.warn("无法找到appId={}, moduleNam={}，对应的接口apiCode={}", duoDocBaseProvider.getAppId(), moduleName, fieldConf.getCode());
            return;
        }

        String queryName = fieldConf.getQueryName();
        if (Strings.isNullOrEmpty(queryName)) {
            queryName = api.getName();
        }

        GraphQLFieldDefinition.Builder graphQLFieldDefinitionBuilder = GraphQLFieldDefinition.newFieldDefinition()
                .name(queryName)
                .type(JsonScalar.INSTANCE);
        if (!Strings.isNullOrEmpty(api.getComment())) {
            graphQLFieldDefinitionBuilder.description(api.getName() + GraphqlConsts.STR_TURN_LINE + api.getComment());
        } else {
            graphQLFieldDefinitionBuilder.description(api.getName());
        }
        List<GraphQLArgument> arguments = Lists.newArrayList();
        List<EntityRef> requestParams = api.getRequestParams();
        if (!CollectionUtils.isEmpty(requestParams)) {
            requestParams.forEach(param -> {
                GraphQLArgument.Builder graphQLArgumentBuilder = GraphQLArgument.newArgument().name(param.getName());
                GraphQLInputType type = (GraphQLInputType) GraphqlTypeUtils.getGraphqlInputType(registryState, moduleName, param.getEntityName());
                Assert.notNull(type, "typ不允许为空：" + param.getEntityName());
                boolean required = param.isRequired() || GraphqlTypeUtils.PATH_VARIABLE.equals(param.getAnnotation()) || GraphqlTypeUtils.REQUEST_BODY.equals(param.getAnnotation());
                if (required) {
                    //必填的
                    graphQLArgumentBuilder.type(GraphQLNonNull.nonNull(type));
                } else {
                    graphQLArgumentBuilder.type(type);
                }

                arguments.add(graphQLArgumentBuilder.build());
            });
            graphQLFieldDefinitionBuilder.arguments(arguments);
        }

        DataFetcherProxy dataFetcher = new DataFetcherProxy(api, getVirtualProvider(moduleName, duoDocBaseProvider));
        dataFetcher.setDataPath(fieldConf.getDataPath());

        if (fieldConf.getActionName() != null && fieldConf.getActionName().equalsIgnoreCase(GraphqlConsts.MUTATION)) {
            registryState.addMutationFieldDefinition(moduleName, graphQLFieldDefinitionBuilder.build());
            registryState.codeRegistry(GraphqlConsts.STR_M + moduleName.toUpperCase(), queryName, dataFetcher);
        } else {
            registryState.addQueryFieldDefinition(moduleName, graphQLFieldDefinitionBuilder.build());
            registryState.codeRegistry(moduleName.toUpperCase(), queryName, dataFetcher);
        }
    }

    private DuoDocGraphqlProviderServiceInfo getVirtualProvider(String moduleName, DuoDocBaseProviderDto duoDocBaseProvider) {
        DuoDocGraphqlProviderServiceInfo provider = new DuoDocGraphqlProviderServiceInfo();
        provider.setModuleName(moduleName);
        provider.setAppId(duoDocBaseProvider.getAppId());
        provider.setServer(duoDocBaseProvider.getServer());
        return provider;
    }

    private String getResourceFileAsString(String resourceName) {
        try (InputStream is = getClass().getClassLoader().getResourceAsStream(resourceName)) {
            if (is != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charsets.UTF_8));
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        } catch (Exception e) {
            throw new GraphqlBuildException("读取资源文件[" + resourceName + "]失败！", e);
        }
        return null;
    }
}
