package com.duoec.graphql.dto.resp;

import java.io.Serializable;

public class User implements Serializable {
    /**
     * ID
     *
     * @demo 34213
     */
    private Long id;

    /**
     * 用户名称
     *
     * @demo 张三
     */
    private String name;

    /**
     * 用户头像
     *
     * @demo http://image.duoec.com/abc.jpg
     */
    private String avatar;

    /**
     * 性别：0=未知 1=男 2=女
     *
     * @demo 1
     */
    private Integer gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
}
