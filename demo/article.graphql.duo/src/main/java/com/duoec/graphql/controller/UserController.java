package com.duoec.graphql.controller;

import com.duoec.graphql.dto.resp.User;
import com.duoec.graphql.provider.annotation.GraphqlModule;
import com.duoec.graphql.provider.annotation.IdProvider;
import com.duoec.graphql.provider.annotation.IdsProvider;
import com.duoec.graphql.provider.annotation.SchemaProvider;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @chapter 用户模块
 * @section 用户信息
 */
@GraphqlModule("user")
@SchemaProvider(clazz = User.class, ids = {"uid", "userId"}) //声明为标准视图，并向外注册了这两个外键
@RestController
//可选，声明当前的领域，如果与配置中的graphql.schema.module值一致，则可以省略，一个GraphQL Provider允许实现多个领域，这也是为了在项目之初某些领域比较小时，先寄放在其它项目里面，可选
@RequestMapping("/api/user")
public class UserController {
    /**
     * 查询某个用户
     *
     * @param id 用户ID
     * @return 用户信息
     */
    @GetMapping("/{id:\\d+}")
    @IdProvider //声明成IdProvider，通过外键关联的就是通过此接口获取到实体的详情信息的
    public User userById(@PathVariable long id) {
        return getUser(id);
    }

    /**
     * 批量查询用户
     *
     * @param ids 用户IDs
     * @return 用户信息列表
     */
    @GetMapping
    @IdsProvider //批量查询接口，引擎检测到可以合并请求时，会合并多个id，调用这个接口。默认分隔符是半角逗号，也可以配置当前注解属性进行修改。批量接口，返回不需要按ids的顺序返回，而是交由引擎处理
    public List<User> userByIds(@RequestParam String ids) {
        List<User> users = Lists.newArrayList();
        Splitter.on(",")
                .omitEmptyStrings()
                .omitEmptyStrings()
                .split(ids)
                .forEach(idStr -> {
                    User user = getUser(Long.parseLong(idStr));
                    users.add(user);
                });
        return users;
    }

    private User getUser(long userId) {
        User user = new User();
        user.setId(userId);
        user.setName("张三");
        user.setAvatar("http://image.duoec.com/" + userId + ".jpg");
        user.setGender(1);
        return user;
    }
}
