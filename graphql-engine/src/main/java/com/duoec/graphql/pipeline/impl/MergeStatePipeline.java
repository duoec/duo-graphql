package com.duoec.graphql.pipeline.impl;

import com.duoec.graphql.core.GraphqlContext;
import com.duoec.graphql.core.util.GraphqlContextUtils;
import com.duoec.graphql.pipeline.RegistryState;
import com.duoec.graphql.core.GraphqlModuleContext;
import com.duoec.graphql.pipeline.Pipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * 将一些公共信息合并到RegistryState
 *
 * @author xuwenzhen
 * @date 2019/6/5
 */
@Service
public class MergeStatePipeline implements Pipeline {
    private static final Logger logger = LoggerFactory.getLogger(MergeStatePipeline.class);
    /**
     * 处理
     *
     * @param registryState 当前注册信息
     */
    @Override
    public void doPipeline(RegistryState registryState) {
        GraphqlContext graphqlContext = GraphqlContextUtils.getGraphqlContext(registryState.getSchemaName());
        Map<String, GraphqlModuleContext> moduleMap = graphqlContext.getModuleContextMap();
        if (CollectionUtils.isEmpty(moduleMap)) {
            return;
        }

        //合并现存的模块(包含Inner Provider)
        moduleMap.forEach((moduleName, moduleContext) -> {
            //检查是否存在
            if (registryState.moduleContextExists(moduleName)) {
                return;
            }
            if (moduleContext.getInnerProvider() == null) {
                //Remote Provider
                registryState.putRemoteProvider(moduleContext);
            } else {
                //Inner Provider
                logger.info("准备注册Inner Provider：{}", moduleContext.getModuleName());
                registryState.putInnerProvider(moduleContext);
            }
        });
    }

    /**
     * 执行的顺序
     *
     * @return 数值，越小越前
     */
    @Override
    public int order() {
        return 200;
    }
}
