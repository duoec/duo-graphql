package com.duoec.graphql.controller;

import com.duoec.graphql.provider.dto.BaseResponse;
import com.duoec.graphql.provider.dto.DuoDocGraphqlProviderServiceInfo;
import com.duoec.graphql.register.GraphqlRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xuwenzhen
 * @chapter Graphql引擎
 * @section Provider注册接口
 * @date 2019/4/9
 */
@RestController
@RequestMapping("/api/register")
public class RegisterApiController {
    @Autowired
    private GraphqlRegister<DuoDocGraphqlProviderServiceInfo> graphqlRegister;

    /**
     * 注册远端数据供应商（Duo-Doc项目）
     *
     * @return
     */
    @PostMapping("/duo-doc")
    public BaseResponse registerWithDuoDoc(@RequestBody DuoDocGraphqlProviderServiceInfo request) {
        graphqlRegister.register(request);
        return BaseResponse.success();
    }

    /**
     * Ping
     * @return
     */
    @GetMapping("/ping")
    public BaseResponse<String> ping() {
        return BaseResponse.success("success");
    }
}
