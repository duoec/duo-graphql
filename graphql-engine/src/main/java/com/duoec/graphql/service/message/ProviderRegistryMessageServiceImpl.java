package com.duoec.graphql.service.message;

import com.duoec.graphql.core.GraphqlConsts;
import com.duoec.graphql.provider.dto.DuoDocGraphqlProviderServiceInfo;
import com.duoec.graphql.register.JsonService;
import com.duoec.graphql.register.server.GraphqlEngineService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Provider 注册消息处理器
 *
 * @author xuwenzhen
 * @date 2019/8/22
 */
@Service
public class ProviderRegistryMessageServiceImpl extends BaseMessageService {
    @Autowired
    private JsonService jsonService;

    @Autowired
    private GraphqlEngineService<DuoDocGraphqlProviderServiceInfo> graphqlEngineService;

    /**
     * 获取当前消息处理的topic或pattern
     *
     * @return topic / pattern
     */
    @Override
    public String getTopic() {
        return getRoot() + GraphqlConsts.STR_APPS  + GraphqlConsts.STR_CLN + GraphqlConsts.STR_SUB;
    }

    @Override
    public void process(String channel, String data) {
        DuoDocGraphqlProviderServiceInfo provider = jsonService.toObject(data, DuoDocGraphqlProviderServiceInfo.class);
        graphqlEngineService.emitProviderList(Lists.newArrayList(provider));
    }
}
