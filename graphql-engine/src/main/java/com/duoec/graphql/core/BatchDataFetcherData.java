package com.duoec.graphql.core;

import com.duoec.graphql.fetcher.DataFetcherProxy;
import com.duoec.graphql.provider.dto.provider.Api;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * 需要进行合并请求的数据
 *
 * @author xuwenzhen
 * @date 2019/7/29
 */
public class BatchDataFetcherData implements Serializable {
    /**
     * 当前模块的上下文
     */
    private final GraphqlModuleContext contextModule;

    /**
     * 合并请求的接口
     */
    private final Api api;

    /**
     * 当前字段绑定的DataFetcher
     */
    private final DataFetcherProxy dataFetcher;

    /**
     * 需要查询的字段
     */
    private List<String> selections;

    /**
     * 字段路径
     */
    private final String fieldPath;

    public BatchDataFetcherData(GraphqlModuleContext contextModule, DataFetcherProxy dataFetcher, Api api, String fieldPath) {
        this.contextModule = contextModule;
        this.dataFetcher = dataFetcher;
        this.api = api;
        this.fieldPath = fieldPath;
    }

    public DataFetcherProxy getDataFetcher() {
        return dataFetcher;
    }

    public Api getApi() {
        return api;
    }

    public GraphqlModuleContext getContextModule() {
        return contextModule;
    }

    public String getFieldPath() {
        return fieldPath;
    }

    public List<String> getSelections() {
        return selections;
    }

    public void setSelections(List<String> selections) {
        this.selections = selections;
    }

    public void addSelection(String selection) {
        if (selections == null) {
            selections = Lists.newArrayList();
        }
        selections.add(selection);
    }
}
